import * as types from './types';

function <%= name %>Request() {
  return {
    type: types.<%= name.toUpperCase() %>_REQUEST,
  };
}
function <%= name %>Success(response) {
  return {
    type: types.<%= name.toUpperCase() %>_SUCCESS,
    payload: response,
  };
}

function <%= name %>Failure(error) {
  return {
    type: types.<%= name.toUpperCase() %>_FAILURE,
    payload: error,
  };
}

export function <%= name %>() {
  return async (dispatch) => {
    dispatch(<%= name %>Request());
    try {
      const response = 'Success';
      dispatch(<%= name %>Success(response));
    } catch (error) {
      dispatch(<%= name %>Failure(error.message));
    }
  };
}
