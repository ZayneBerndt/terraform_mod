import React from 'react';

import axios from 'axios';

export default class Version extends React.Component {
  state = {
    version: 0
  }

  componentDidMount() {
    axios.get('/api/version')
      .then(res => {
        const version = res.data.number;
        this.setState({ version });
      })
  }

  render() {
    return (
      <div>
        {`Version is: ${this.state.version}` }
      </div>
    )
  }
}
