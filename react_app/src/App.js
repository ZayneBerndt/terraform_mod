import React, { Component } from 'react';
import Version from './components/version/version.js'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Version />
      </div>
    );
  }
}

export default App;
