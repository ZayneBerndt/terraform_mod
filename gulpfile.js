const gulp         = require('gulp');
const babel        = require('gulp-babel');
const imagemin     = require('gulp-imagemin');
const concat       = require('gulp-concat');
const browserSync  = require('browser-sync');
const eslint       = require('gulp-eslint');
const plumber      = require('gulp-plumber');
const sourcemaps   = require('gulp-sourcemaps');
const sass         = require('gulp-sass');
const template     = require('gulp-template');
const { argv } = require('yargs');
var run = require('gulp-run');

var onError = function(err) {
  notify.onError({
    title:    "Error",
    message:  "<%= error %>",
  })(err);
  this.emit('end');
};
//
var plumberOptions = {
  errorHandler: onError,
};
//copy all html files
gulp.task("copyHtml", function(){
  gulp.src('./src/*.html')
    .pipe(gulp.dest('dist'));
});
//Optimize images
gulp.task('imagemin', function() {
    gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
});
//collect all js files at once
var jsFiles = {
  source: [
    './src/state/*.js',
    './src/state/*.jsx',
  ]
};
//run lint against js and jsx code
gulp.task('eslint', function() {
  return gulp.src(jsFiles.source)
    .pipe(eslint({
      baseConfig: {
        "ecmaFeatures": {
           "jsx": true
         }
      }
    }))
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});
var JEST_OPTIONS = {
    scriptPreprocessor: "./node_modules/babel-jest",
    testPathDirs: [
        "src/js/**/*.spec.js"
    ],
    unmockedModulePathPatterns: [
        "src/node_modules/*"
    ],
    moduleFileExtensions: [
      "js",
      "json",
      "jsx"
    ],
    testFileExtensions: [
      ".spec.js",
      ".spec.jsx"
    ]
}
// Concatenate jsFiles.source into one JS file.
// Run copy-react and eslint before concatenating
gulp.task('concat', function() {
  return gulp.src(jsFiles.source)
    .pipe(sourcemaps.init())
    .pipe(babel({
      only: [
        'assets/state/src/components',
      ],
      compact: false
    }))
    .pipe(concat('app.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('src/dist'));
});
// Compile Sass to CSS
gulp.task('sass', function() {
  var autoprefixerOptions = {
    browsers: ['last 2 versions'],
  };
  var filterOptions = '**/*.css';
  var reloadOptions = {
    stream: true,
  };
  var sassOptions = {
    includePaths: [
    ]
  };
  return gulp.src('src/sass/**/*.scss')
      .pipe(plumber(plumberOptions))
      .pipe(sourcemaps.init())
      .pipe(sass(sassOptions))
      .pipe(autoprefixer(autoprefixerOptions))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('assets/css'))
      .pipe(filter(filterOptions))
      .pipe(reload(reloadOptions));
  });
//
// Watch JS/JSX files and changed test files
gulp.task('watch', function() {
  gulp.watch('src/state/**/*.{js,jsx}', ['concat']);
  gulp.watch('src/sass/**/*.scss', ['sass']);
});
// BrowserSync
gulp.task('browsersync', function() {
  browserSync({
    server: {
      baseDir: './app.js'
    },
    open: false,
    online: false,
    notify: false,
  });
});

gulp.task('craete', () => {
  const { name } = argv;

  if (name && name.length > 0) {
    const destinationPath = `src/state/${name}`;

    gulp.src('templates/actions.js')
      .pipe(template({ name }))
      .pipe(gulp.dest(destinationPath));

    gulp.src('templates/reducers.js')
      .pipe(template({ name }))
      .pipe(gulp.dest(destinationPath));

    gulp.src('templates/types.js')
      .pipe(template({ name }))
      .pipe(gulp.dest(destinationPath));

    gulp.src('templates/view.html')
      .pipe(template({ name }))
      .pipe(gulp.dest(destinationPath));

    gulp.src('templates/style.css')
      .pipe(template({ name }))
      .pipe(gulp.dest(destinationPath));
    return;
  }
});

gulp.task('build', function() {
  return run('npm run-script build').exec()
});
// gulp.task('default', ['build', 'browsersync', 'watch']);
// gulp.task('build', ['jest', 'concat', 'browsersync', 'eslint']);
// gulp.task('default', ['build', 'browsersync', 'watch']);
